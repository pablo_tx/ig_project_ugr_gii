// #############################################################################
//
// Informática Gráfica (Grado Informática)
//
// Archivo: GrafoParam.h
// -- declaraciones de clase para el objeto jerárquico de la práctica 3
//
// #############################################################################

#ifndef GRAFOPARAM_H_INCLUDED
#define GRAFOPARAM_H_INCLUDED

#include "malla.h" // añadir .h de cualquier objetos malla indexada usados....

constexpr int num_parametros = 16; // número de parámetros o grados de libertad
    // de este modelo

class GrafoParam {
public:
    // crea mallas indexadas (nodos terminales del grafo)
    GrafoParam();

    // función principal de visualización
    void draw(const ModoVis p_modo_vis, const bool p_usar_diferido);

    // actualizar valor efectivo de un parámetro (a partir de su valor no acotado)
    void actualizarValorEfe(const unsigned iparam, const float valor_na);

    // devuelve el número de parámetros
    unsigned numParametros() { return num_parametros; }

private:
    // métodos de dibujo de subgrafos
    void arm_r();
    void arm_l();
    void head_full();
    void upper_body();
    void leg_r();
    void leg_l();
    void fullBody();

    // objetos tipo malla indexada (nodos terminales)
    ObjPLY* ankle_l = nullptr;
    ObjPLY* ankle_r = nullptr;
    ObjPLY* elbow_l = nullptr;
    ObjPLY* elbow_r = nullptr;
    ObjPLY* hand_l = nullptr;
    ObjPLY* hand_r = nullptr;
    ObjPLY* head = nullptr;
    ObjPLY* hip_l = nullptr;
    ObjPLY* hip = nullptr;
    ObjPLY* hip_r = nullptr;
    ObjPLY* hip_tor = nullptr;
    ObjPLY* knee_l = nullptr;
    ObjPLY* knee_r = nullptr;
    ObjPLY* lower_arm_l = nullptr;
    ObjPLY* lower_arm_r = nullptr;
    ObjPLY* lower_leg_l = nullptr;
    ObjPLY* lower_leg_r = nullptr;
    ObjPLY* neck = nullptr;
    ObjPLY* shoulder_l = nullptr;
    ObjPLY* shoulder_r = nullptr;
    ObjPLY* feet_l = nullptr;
    ObjPLY* feet_r = nullptr;
    ObjPLY* body = nullptr;
    ObjPLY* upper_arm_l = nullptr;
    ObjPLY* upper_arm_r = nullptr;
    ObjPLY* upper_leg_l = nullptr;
    ObjPLY* upper_leg_r = nullptr;
    ObjPLY* wrist_l = nullptr;
    ObjPLY* wrist_r = nullptr;

    // parámetros de la llamada actual (o última) a 'draw'
    ModoVis modo_vis; // modo de visualización
    bool usar_diferido; // modo de envío (true -> diferido, false -> inmediato)

    // valores efectivos de los parámetros (angulos, distancias, factores de
    // escala, etc.....) calculados a partir de los valores no acotados
    float head_size,
        upper_leg_rotation, // rotacion piernas
        left_lower_leg_rotation, // rotacion rodilla izquierda
        right_lower_leg_rotation, // rotacion rodilla derecha
        fullbody_y; // altura de la zancada

    std::vector<float> body_rotations;
    // TODO
    //std::vector<int> body_limits;
};

#endif
