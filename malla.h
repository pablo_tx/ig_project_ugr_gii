// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
//
// Informática Gráfica (Grado Informática)
//
// Archivo: ObjMallaIndexada.h
// -- declaraciones de clase ObjMallaIndexada (mallas indexadas) y derivados
//
// #############################################################################

#ifndef OBJETOS_H_INCLUDED
#define OBJETOS_H_INCLUDED

#include "aux.h"

enum ModoVis { ajedrez,
    normal };

struct Material {
    Tupla4f ambient;
    Tupla4f diffuse;
    Tupla4f specular;
    GLfloat shininess;
};

// *****************************************************************************
//
// clase para objetos 3D (mallas indexadas)
//
// *****************************************************************************
class ObjMallaIndexada {
public:
    // dibuja el objeto en modo inmediato
    void draw_ModoInmediato(ModoVis modo_vis);

    // dibuja el objeto en modo diferido (usando VBOs)
    void draw_ModoDiferido(ModoVis modo_vis);

    void draw_seleccion(ModoVis modo_vis);

    // función que redibuja el objeto
    // está función llama a 'draw_MI' (modo inmediato)
    // o bien a 'draw_MD' (modo diferido, VBOs)
    void draw(ModoVis modo_vis, bool modo_dibujado);

    void seleccionar();

    void preparaTextura(); // prepara la textura jpg (práctica 4)

protected:
    void calcular_normales(); // calcula tabla de normales de vértices (práctica 3)
    void calcular_normales_vertices(); // calcula tabla de normales de vértices (práctica 3)
    void aplicar_materiales(); // calcula tabla de normales de vértices (práctica 3)
    
    std::vector<Tupla3f> vertices; // tabla de coordenadas de vértices (una tupla por vértice, con tres floats)
    std::vector<Tupla3i> triangulos; // una terna de 3 enteros por cada cara o triángulo
    GLuint id_vbo_ver = 0;
    GLuint id_vbo_tri = 0;

    std::vector<Tupla3f> normales_caras; // una terna de 3 float por cada cara o triángulo
    std::vector<Tupla3f> normales_vertices; // una terna de 3 float por cada vertice

    GLuint id_textura;
    std::vector<Tupla2f> textura_vertices; // una terna de 2 int por cada vertice
    std::string nombre_archivo;

    bool seleccionado = false;

    std::vector<Tupla3f> color;

    std::vector<Tupla3f> color_anterior;

    Material material; // Struct de materiales

    Material material_anterior;

    Material Brass{
        { 0.329, 0.224, 0.027, 1.0 },
        { 0.78, 0.569, 0.114, 1.0 },
        { 0.992, 0.941, 0.808, 1.0 },
        27.897,
    };
    Material Bronze{
        { 0.213, 0.128, 0.054, 1.0 },
        { 0.714, 0.428, 0.181, 1.0 },
        { 0.394, 0.272, 0.167, 1.0 },
        25.6,
    };
    Material Chrome{
        { 0.25, 0.25, 0.25, 1.0 },
        { 0.4, 0.4, 0.4, 1.0 },
        { 0.775, 0.775, 0.775, 1.0 },
        76.8,
    };
    Material Copper{
        { 0.191, 0.074, 0.023, 1.0 },
        { 0.704, 0.27, 0.083, 1.0 },
        { 0.257, 0.138, 0.086, 1.0 },
        12.8,
    };
    Material Emerald{
        { 0.022, 0.175, 0.022, 1.0 },
        { 0.076, 0.614, 0.076, 1.0 },
        { 0.633, 0.728, 0.633, 1.0 },
        76.8,
    };
    Material Gold{
        { 0.247, 0.2, 0.075, 1.0 },
        { 0.752, 0.606, 0.226, 1.0 },
        { 0.628, 0.556, 0.366, 1.0 },
        51.2,
    };
    Material Jade{
        { 0.135, 0.223, 0.158, 1.0 },
        { 0.54, 0.89, 0.63, 1.0 },
        { 0.316, 0.316, 0.316, 1.0 },
        12.8,
    };
    Material Obsidian{
        { 0.054, 0.05, 0.066, 1.0 },
        { 0.183, 0.17, 0.225, 1.0 },
        { 0.333, 0.329, 0.346, 1.0 },
        38.4,
    };
    Material Pearl{
        { 0.25, 0.207, 0.207, 1.0 },
        { 1, 0.829, 0.829, 1.0 },
        { 0.297, 0.297, 0.297, 1.0 },
        11.264,
    };
    Material PlasticBlack{
        { 0, 0, 0, 1.0 },
        { 0.01, 0.01, 0.01, 1.0 },
        { 0.5, 0.5, 0.5, 1.0 },
        32,
    };
    Material PlasticCyan{
        { 0, 0.1, 0.06, 1.0 },
        { 0, 0.51, 0.51, 1.0 },
        { 0.502, 0.502, 0.502, 1.0 },
        32,
    };
    Material PlasticGreen{
        { 0, 0, 0, 1.0 },
        { 0.1, 0.35, 0.1, 1.0 },
        { 0.45, 0.55, 0.45, 1.0 },
        32,
    };
    Material PlasticRed{
        { 0, 0, 0, 1.0 },
        { 0.5, 0, 0, 1.0 },
        { 0.7, 0.6, 0.6, 1.0 },
        32,
    };
    Material PlasticWhite{
        { 0, 0, 0, 1.0 },
        { 0.55, 0.55, 0.55, 1.0 },
        { 0.7, 0.7, 0.7, 1.0 },
        32,
    };
    Material PlasticYellow{
        { 0, 0, 0, 1.0 },
        { 0.5, 0.5, 0, 1.0 },
        { 0.6, 0.6, 0.5, 1.0 },
        32,
    };
    Material RubberBlack{
        { 0.02, 0.02, 0.02, 1.0 },
        { 0.01, 0.01, 0.01, 1.0 },
        { 0.4, 0.4, 0.4, 1.0 },
        10,
    };
    Material RubberCyan{
        { 0, 0.05, 0.05, 1.0 },
        { 0.4, 0.5, 0.5, 1.0 },
        { 0.04, 0.7, 0.7, 1.0 },
        10,
    };
    Material RubberGreen{
        { 0, 0.05, 0, 1.0 },
        { 0.4, 0.5, 0.4, 1.0 },
        { 0.04, 0.7, 0.04, 1.0 },
        10,
    };
    Material RubberRed{
        { 0.05, 0, 0, 1.0 },
        { 0.5, 0.4, 0.4, 1.0 },
        { 0.7, 0.04, 0.04, 1.0 },
        10,
    };
    Material RubberWhite{
        { 0.05, 0.05, 0.05, 1.0 },
        { 0.5, 0.5, 0.5, 1.0 },
        { 0.7, 0.7, 0.7, 1.0 },
        10,
    };
    Material RubberYellow{
        { 0.05, 0.05, 0, 1.0 },
        { 0.5, 0.5, 0.4, 1.0 },
        { 0.7, 0.7, 0.04, 1.0 },
        10,
    };
    Material Ruby{
        { 0.175, 0.012, 0.012, 1.0 },
        { 0.614, 0.041, 0.041, 1.0 },
        { 0.728, 0.627, 0.627, 1.0 },
        76.8,
    };
    Material Silver{
        { 0.192, 0.192, 0.192, 1.0 },
        { 0.508, 0.508, 0.508, 1.0 },
        { 0.508, 0.508, 0.508, 1.0 },
        51.2,
    };
    Material Turquoise{
        { 0.1, 0.187, 0.175, 1.0 },
        { 0.396, 0.742, 0.691, 1.0 },
        { 0.297, 0.308, 0.307, 1.0 },
        12.8,
    };
    Material VIC{
        { 0.19225,0.19225 ,0.29225,1 },
        { 0.30754,0.50754,0.50754,1 },
        { 0.508273,0.508273,0.508273,1 },
        10,
    };
};

// *****************************************************************************
//
// clases derivadas de ObjMallaIndexada (definen constructores específicos)
//
// *****************************************************************************

// *****************************************************************************
// Cubo con centro en el origen y lado unidad
// (tiene 9 vertices y 6 caras)
class Cubo : public ObjMallaIndexada {
public:
    Cubo();
};

// *****************************************************************************
// Tetraedro
//
class Tetraedro : public ObjMallaIndexada {
public:
    Tetraedro();
};

// *****************************************************************************
// objeto leído de un archivo PLY
class ObjPLY : public ObjMallaIndexada {
public:
    ObjPLY(const std::string& nombre_archivo);
};

// *****************************************************************************
// objeto de revolución obtenido a partir de un perfil (en un PLY)
class ObjRevolucion : public ObjMallaIndexada {
public:
    ObjRevolucion() {}
    ObjRevolucion(const std::string& nombre_ply_perfil);

protected:
    void crearMalla(const std::vector<Tupla3f>& perfil_original, const int num_instancias_perf);
    void ponerTapas(float altura, const int num_instancias_perf);

private:
    const int NUM_INSTANCIAS_DEFAULT = 10;
};

// *****************************************************************************
// Cono
//
class Cono : public ObjRevolucion {
public:
    Cono(const int num_vert_perfil, const int num_instancias_perf);
};

// *****************************************************************************
// Cilindro
//
class Cilindro : public ObjRevolucion {
public:
    Cilindro(const int num_vert_perfil, const int num_instancias_perf);
};

// *****************************************************************************
// Esfera
//
class Esfera : public ObjRevolucion {
public:
    Esfera(const int num_vert_perfil, const int num_instancias_perf);
};

// *****************************************************************************
// Clase Cuadro
class Cuadro : public ObjMallaIndexada {
public:
    Cuadro(const std::string& nombre_archivo, const std::vector<Tupla2f> textura_vertices);
};

#endif
