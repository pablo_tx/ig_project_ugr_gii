// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#ifndef _ESCENA_H
#define _ESCENA_H

#include "ejes.h"
#include "jerarquico.h"
#include "malla.h"
#include "luz.h"
#include "camara.h"

class Escena {

private:
    Ejes ejes;

    // variables que definen la posicion de la camara en coordenadas polares
    GLfloat Observer_distance;
    GLfloat Observer_angle_x;
    GLfloat Observer_angle_y;

    // variables que controlan la ventana y la transformacion de perspectiva
    GLfloat Width, Height, Front_plane, Back_plane;

    void clear_window();
    void dibujar_objeto_actual();

    // Transformación de cámara
    void change_projection(const float ratio_xy);
    void change_observer();

    int objeto_actual = 9, // objeto actual (el que se visualiza)
        num_objetos = 9,
        num_modos = 3,
        modo_visualizacion = 2; // número de objetos (actualizado al crear los objetos en el constructor)

    bool modo_dibujado = false; // True Diferido, False Inmediato
    bool shadeModel = true; // True GL_SMOOTH, False GL_FLAT

    // Objetos de la escena
    // P1
    Cubo* cubo = nullptr; // es importante inicializarlo a 'nullptr'
    Tetraedro* tetraedro = nullptr;
    
    // P2
    Cono* cono = nullptr;
    Cilindro* cilindro = nullptr;
    Esfera* esfera = nullptr;
    ObjPLY* ply = nullptr;
    ObjRevolucion* plyrevolucion = nullptr;

    // P3 
    ObjJerarquico* jerarquico = nullptr;
    bool animacionActiva = false;

    // P4
    Tupla4f ambiente = {0.0,0.0,0.0,1.0};

    Tupla4f posicion_direccional = {0.0,0.0,1.0,0.0};
    Tupla4f difusa_direccional = {1.0,1.0,1.0,1.0};
    Tupla4f especular_direccional = {1.0,1.0,1.0,1.0};
    
    Tupla4f posicion_puntual = {10.0,0.0,0.0,1.0};
    Tupla4f difusa_puntual = {1.0,0.0,1.0,1.0};
    Tupla4f especular_puntual = {1.0,0.0,1.0,1.0};
    
    Tupla4f posicion_puntual2 = {0.0,1.0,0.0,0.0};
    Tupla4f difusa_puntual2 = {1.0,0.0,0.0,1.0};
    Tupla4f especular_puntual2 = {1.0,0.0,0.0,1.0};
    
    Luz* luz_infinita = nullptr;
    Luz* luz_puntual_magenta = nullptr;
    Luz* luz_puntual2 = nullptr;

    Cuadro* cuadro = nullptr;

    //P5
    bool left_clicking = false, right_clicking = false;
    int x_clickpos = 0, y_clickpos = 0, yaw = 0, pitch = 0;
    Camara *camaras[2];
    int camaraActiva = 0;
    GLubyte colores[6][3] = { {255,100,100}, {230,50,50}, {210,50,100}, {180,80,80}, {140,80,80}, {100,80,80}};

    void pick(int x, int y);

public:
    Escena();
    void inicializar(int UI_window_width, int UI_window_height);
    void redimensionar(int newWidth, int newHeight);

    // Dibujar
    void dibujar();
    void dibujaNormal();
    void dibujaSeleccion();

    // Interacción con la escena
    bool teclaPulsada(unsigned char Tecla1, int x, int y);
    void teclaEspecial(int Tecla1, int x, int y);

    void clickRaton( int boton, int estado, int x, int y );
    void ratonMovido( int x, int y );

    // P3
    void conmutarAnimaciones();
    void mgeDesocupado();
};
#endif
