// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#include "escena.h"
#include "aux.h" // includes de OpenGL/glut/glew, windows, y librería std de C++
#include "luz.h"
#include "camara.h"
#include "malla.h" // objetos: Cubo y otros....


//**************************************************************************
// constructor de la escena (no puede usar ordenes de OpenGL)
//**************************************************************************

Escena::Escena()
{
    Front_plane = 0.1;
    Back_plane = 2000.0;
    Observer_distance = 2.0;
    
    ejes.changeAxisSize(5000);

    // crear los objetos de las prácticas: Mallas o Jerárquicos....
    // P1
    cubo = new Cubo();
    tetraedro = new Tetraedro();
    
    // P2
    cono = new Cono(20, 20);
    cilindro = new Cilindro(20, 20);
    esfera = new Esfera(20, 20);
    ply = new ObjPLY("plys/beethoven.ply");
    plyrevolucion = new ObjRevolucion("plys/peon.ply");
    
    // P3
    jerarquico = new ObjJerarquico();

    // P4
    luz_infinita = new Luz(GL_LIGHT0, posicion_direccional, ambiente, difusa_direccional, especular_direccional);
    luz_puntual_magenta = new Luz(GL_LIGHT1, posicion_puntual, ambiente, difusa_puntual, especular_puntual);
    luz_puntual2 = new Luz(GL_LIGHT2, posicion_puntual2, ambiente, difusa_puntual2, especular_puntual2);
    
    std::vector<Tupla2f> cuadro_vertices = {
        { 0, 1 }, // 0
        { 0, 0 }, // 1
        { 1, 0 }, // 2
        { 1, 1 }, // 3
    };
    cuadro = new Cuadro("./garner-narrative.jpg", cuadro_vertices);
    
    camaras[0] = new Camara(0, 5, Back_plane, 20);
    camaras[1] = new Camara(1, 5, Back_plane, 20);
}

//**************************************************************************
// inicialización de la escena (se ejecuta cuando ya se ha creado la ventana, por
// tanto sí puede ejecutar ordenes de OpenGL)
// Principalmemnte, inicializa OpenGL y la transf. de vista y proyección
//**************************************************************************

void Escena::inicializar(int UI_window_width, int UI_window_height)
{
    glClearColor(1.0, 1.0, 1.0, 1.0); // se indica cual sera el color para limpiar la ventana	(r,v,a,al)

    glEnable(GL_DEPTH_TEST); // se habilita el z-bufer

    // Activar las luces
    glEnable(GL_LIGHTING);

    redimensionar(UI_window_width, UI_window_height);

    

    // Activar por defecto el modo suave
    glShadeModel(GL_SMOOTH);

    glEnable(GL_NORMALIZE);

    cuadro->preparaTextura();
}

// ***************************************if(dzoom > 0){***********************************
// Funcion que dibuja el objeto activo actual, usando su método 'draw'
// (llamada desde Escena::dibujar)
// ***************************************************************************
void Escena::dibujar_objeto_actual()
{
    using namespace std;
    
    ModoVis modo_vis = normal;

    glPointSize(1);
    
    switch (modo_visualizacion) {
        
    case 0:
        // Puntos
        glPointSize(5);
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        break;
    case 1:
        // Lineas
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        break;
    case 2:
        // Relleno
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        break;
    case 3:
        // Ajedrez
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        modo_vis = ajedrez;
        break;
    default:
        cout << "draw_object: el modo de visualizacion actual (" << modo_visualizacion << ") es incorrecto." << endl;
        break;
    }

    // (2) dibujar el objeto actual usando método 'draw' del objeto asociado al
    // valor entero en 'objeto_actual'

    switch (objeto_actual) {
    case 0:
        if (cubo != nullptr)
            cubo->draw(modo_vis, modo_dibujado);
        break;
    case 1:
        if (tetraedro != nullptr)
            tetraedro->draw(modo_vis, modo_dibujado);
        break;
    case 2:
        if (cono != nullptr)
            cono->draw(modo_vis, modo_dibujado);
        break;
    case 3:
        if (cilindro != nullptr)
            cilindro->draw(modo_vis, modo_dibujado);
        break;
    case 4:
        if (esfera != nullptr)
            esfera->draw(modo_vis, modo_dibujado);
        break;
    case 5:
        if (ply != nullptr)
            ply->draw(modo_vis, modo_dibujado);
        break;
    case 6:
        if (plyrevolucion != nullptr)
            plyrevolucion->draw(modo_vis, modo_dibujado);
        break;
    case 7:
        if (jerarquico != nullptr)
            jerarquico->draw(modo_vis, modo_dibujado);
        break;
    case 8:
        if (cuadro != nullptr)
            cuadro->draw(modo_vis, modo_dibujado);
        break;
    case 9:
        dibujaNormal();
        break;
    default:
        cout << "draw_object: el número de objeto actual (" << objeto_actual << ") es incorrecto." << endl;
        break;
    }
    
}

// función de dibujo de la escena: limpia ventana, fija cámara, dibuja ejes,
// y dibuja los objetos
//
// **************************************************************************
void Escena::dibujar()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Limpiar la pantalla
    
    change_observer();
    ejes.draw();
    
    dibujar_objeto_actual();
    
    // Encender la luz infinita
    luz_infinita->conmutar(true);
    
    // Encender la luz direccional
    luz_puntual_magenta->conmutar(true);
    //luz_puntual2->conmutar(true);
}

void Escena::dibujaSeleccion()
{
    glDisable(GL_DITHER);
    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
        glPushMatrix();
        switch(i*3+j){
            case 0: glColor3ub(colores[0][0],colores[0][1],colores[0][2]);break;
            case 1: glColor3ub(colores[1][0],colores[1][1],colores[1][2]);break;
            case 2: glColor3ub(colores[2][0],colores[2][1],colores[2][2]);break;
            case 3: glColor3ub(colores[3][0],colores[3][1],colores[3][2]);break;
            case 4: glColor3ub(colores[4][0],colores[4][1],colores[4][2]);break;
            case 5: glColor3ub(colores[5][0],colores[5][1],colores[5][2]);break;
        }
        glTranslatef(i*3.0,i*2,-j*3.0);
        switch(i*3+j){
            case 0: tetraedro->draw_seleccion(normal); break;
            case 1: cilindro->draw_seleccion(normal); break;
            case 2: esfera->draw_seleccion(normal); break;
            case 3: plyrevolucion->draw_seleccion(normal); break;
            case 4: cuadro->draw_seleccion(normal); break;
            case 5: cono->draw_seleccion(normal); break;
        }
        glPopMatrix();
        }
    }
    
}

void Escena::pick(int x, int y){
    
    GLubyte pixels[3];
    GLint viewport[4];
    bool lighting = false;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Limpiar la pantalla
    if (glIsEnabled(GL_LIGHTING)) {
        lighting = true;
    }
    
    dibujaSeleccion();
    glGetIntegerv(GL_VIEWPORT, viewport);
    
    glReadPixels(x, viewport[3]-y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    if (lighting) {
        glEnable(GL_LIGHTING);
    }

    for(int i = 0; i < 6; ++i){
        //std::cout << colores[i][0] << colores[i][1] << colores[i][2] << std::endl;
        if((int)pixels[0] == (int)colores[i][0] && (int)pixels[1] == (int)colores[i][1] && (int)pixels[2] == (int)colores[i][2]){
            switch(i){
            case 0: tetraedro->seleccionar(); break;
            case 1: cilindro->seleccionar(); break;
            case 2: esfera->seleccionar(); break;
            case 3: plyrevolucion->seleccionar(); break;
            case 4: cuadro->seleccionar(); break;
            case 5: cono->seleccionar(); break;
            }
        }
    }
    //std::cout << (int)pixels[0] << (int)pixels[1] << (int)pixels[2] << std::endl;
    //std::cout << x << " " << y << std::endl;
}

void Escena::dibujaNormal()
{
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
        glPushMatrix();
        glTranslatef(i*3.0,i*2,-j*3.0);
        switch(i*3+j){
            case 0: tetraedro->draw(normal, modo_dibujado); break;
            case 1: cilindro->draw(normal, modo_dibujado); break;
            case 2: esfera->draw(normal, modo_dibujado); break;
            case 3: plyrevolucion->draw(normal, modo_dibujado); break;
            case 4: cuadro->draw(normal, modo_dibujado); break;
            case 5: cono->draw(normal, modo_dibujado); break;
        }
        glPopMatrix();
        }
    }
  
}

//**************************************************************************
//
// función que se invoca cuando se pulsa una tecla
// Devuelve true si se ha pulsado la tecla para terminar el programa (Q),
// devuelve false en otro caso.
//
//**************************************************************************
bool Escena::teclaPulsada(unsigned char tecla, int x, int y)
{
    using namespace std;
    cout << "Tecla pulsada: '" << tecla << "'" << endl;

    switch (toupper(tecla)) {
    case 'Q':
        // salir
        return true;
        break;
    case 'O':
        // activar siguiente objeto
        objeto_actual = (objeto_actual + 1) % num_objetos;
        cout << "Objeto actual == " << objeto_actual << endl;
        break;
    case 'V':
        // Cambiar modo de dibujado
        modo_dibujado = !modo_dibujado;

        if (modo_dibujado) {
            cout << "Modo de dibujado diferido" << endl;
        } else {
            cout << "Modo de dibujado inmediato" << endl;
        }
        break;
    case 'M':
        // Cambiar modo de visualización
        modo_visualizacion = (modo_visualizacion + 1) % num_modos;
        cout << "Modo de visualizacion == " << modo_visualizacion << endl;
        break;
    case 'P':
        // Siguiente Parametro
        jerarquico->siguienteParametro();
        cout << "Siguiente Parametro" << endl;
        break;
    case 'K':
        // Anterior Parametro
        jerarquico->anteriorParametro();
        cout << "Anterior Parametro" << endl;
        break;
    case 'A':
        // Conmutar Animaciones
        conmutarAnimaciones();
        cout << "Animaciones Conmutadas" << endl;
        break;
    case 'Z':
        // Incrementar Parametro Actual
        jerarquico->incrementaParamAct();
        cout << "Incrementado Parametro Actual" << endl;
        break;
    case 'X':
        // Decrementar Parametro Actual
        jerarquico->decrementaParamAct();
        cout << "Decrementado Parametro Actual" << endl;
        break;
    case '>':
        // Acelerar
        jerarquico->acelerar();
        cout << "Acelerar" << endl;
        break;
    case '<':
        // Decelerar
        jerarquico->decelerar();
        cout << "Decelerar" << endl;
        break;
    case 'L':
        // Modo Lineas
        modo_visualizacion = 1;
        cout << "Modo Lineas (1)" << endl;
        break;
    case 'F':
        // Modo Relleno
        modo_visualizacion = 2;
        cout << "Modo Relleno (2)" << endl;
        break;
    case 'C':
        // Modo Ajedrez
        modo_visualizacion = 3;
        cout << "Modo Ajedrez (3)" << endl;
        break;
    case 'H':
        // Modo suavizado
        if (shadeModel) {
            glShadeModel(GL_FLAT);
            shadeModel = !shadeModel;
            cout << "Modo suavizado GL_FLAT" << endl;
        } else {
            shadeModel = !shadeModel;
            glShadeModel(GL_SMOOTH);
            cout << "Modo suavizado GL_SMOOTH" << endl;
        }
        break;
    case 'G':
        // Desactivar/Activar luces
        if (glIsEnabled(GL_LIGHTING)) {
            glDisable(GL_LIGHTING);
        } else {
            glEnable(GL_LIGHTING);
        }
        break;
    case '1':
        // Cubo
        objeto_actual = 0;
        cout << "Objeto Cubo (objeto_actual 0)" << endl;
        break;
    case '2':
        // Cono
        objeto_actual = 2;
        cout << "Objeto Cono (objeto_actual 2)" << endl;
        break;
    case '3':
        // Cilindro
        objeto_actual = 3;
        cout << "Objeto Cilindro (objeto_actual 3)" << endl;
        break;
    case '4':
        // Esfera
        objeto_actual = 4;
        cout << "Objeto Esfera (objeto_actual 4)" << endl;
        break;
    case '5':
        // PLY
        objeto_actual = 5;
        cout << "Objeto PLY (objeto_actual 5)" << endl;
        break;
    case '6':
        // Revolucion PLY
        objeto_actual = 6;
        cout << "Objeto Revolucion PLY (objeto_actual 6)" << endl;
        break;
    case '7':
        // Objeto Jerarquico
        objeto_actual = 7;
        cout << "Objeto Jerarquico (objeto_actual 7)" << endl;
        break;
    case '8':
        // Objeto Cuadro
        objeto_actual = 8;
        cout << "Objeto Cuadro (objeto_actual 8)" << endl;
        break;
    case '9':
        // Objeto
        objeto_actual = 9;
        cout << "Objeto Cuadro (objeto_actual 9)" << endl;
        break;
    }

    return false;
}

//**************************************************************************
void Escena::conmutarAnimaciones()
{
    using namespace std;
    animacionActiva = !animacionActiva;
    if (animacionActiva) {
        jerarquico->inicioAnimaciones();
        glutIdleFunc(funcion_desocupado);
    } else {
        glutIdleFunc(nullptr);
    }
}

//**************************************************************************
void Escena::mgeDesocupado()
{
    luz_puntual_magenta->rotar(5, 0, 1, 0);
    //luz_puntual2->rotar(-5, 0,1,0);

    jerarquico->actualizarEstado();
    glutPostRedisplay();
}

//**************************************************************************
void Escena::teclaEspecial(int Tecla, int x, int y)
{
    switch (Tecla) {
    /*case GLUT_KEY_LEFT:
        camaras[camaraActiva]->rotarX(+5);
        break;3
    case GLUT_KEY_LEFT:
        camaras[camaraActiva]->rotarX(-5);
        break;3
    case GLUT_KEY_UP:
        camaras[camaraActiva]->rotarY(5);
        break;
    case GLUT_KEY_DOWN:
        camaras[camaraActiva]->rotarY(-5);
        break;*/
    case GLUT_KEY_PAGE_UP:
        camaras[camaraActiva]->zoom(1.2);
        break;
    case GLUT_KEY_PAGE_DOWN:
        camaras[camaraActiva]->zoom(-1.2);
        break;
    case GLUT_KEY_F1:
        this->camaraActiva = 0;
        this->redimensionar(Width, Height);
        break;
    case GLUT_KEY_F2:
        this->camaraActiva = 1;
        this->redimensionar(Width, Height);
        break;
    }
}

//**************************************************************************
// Funcion para definir la transformación de proyeccion
//
// ratio_xy : relacción de aspecto del viewport ( == ancho(X) / alto(Y) )
//
//***************************************************************************
void Escena::change_projection(const float ratio_xy)
{
    const float wy = 0.84 * Front_plane,
                wx = ratio_xy * wy;
    camaras[camaraActiva]->setProyeccion(wx, wy);
}

//**************************************************************************
// Funcion que se invoca cuando cambia el tamaño de la ventana
//***************************************************************************
void Escena::redimensionar(int newWidth, int newHeight)
{
    Width = newWidth;
    Height = newHeight;
    change_projection(float(Width) / float(Height));
    glViewport(0, 0, Width, Height);
}

//**************************************************************************
// Funcion para definir la transformación de vista (posicionar la camara)
//***************************************************************************
void Escena::change_observer()
{
    // posicion del observador
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    camaras[camaraActiva]->setObservador();
}

void Escena::clickRaton(int boton, int estado, int x, int y)
{
    switch(boton){
        case GLUT_RIGHT_BUTTON:
            if (estado == GLUT_DOWN) {
                right_clicking = true;
                x_clickpos = x;
                y_clickpos = y;
            } else {
                right_clicking = false;
            }
            break;
        case GLUT_LEFT_BUTTON:
            if (estado == GLUT_DOWN) {
                left_clicking = true;
                x_clickpos = x;
                y_clickpos = y;
                pick(x, y);
            } else left_clicking = false;
            break;
        case 3:
            camaras[camaraActiva]->zoom(1.2);
            break;
        case 4:
            camaras[camaraActiva]->zoom(-1.2);
            break;
        default:
            break;
    }
}

void Escena::ratonMovido(int x, int y)
{
    if (right_clicking) {
        yaw += x-x_clickpos;
        pitch -= y_clickpos-y;
        x_clickpos = x;
        y_clickpos = y;
        camaras[camaraActiva]->girar(pitch, yaw);
    }
    glutPostRedisplay();
}
