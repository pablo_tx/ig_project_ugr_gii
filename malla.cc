// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#define cimg_use_jpeg
#define cimg_display 0
#include "CImg.h"
#include "malla.h"
#include "aux.h"
#include "ply_reader.h"


// *****************************************************************************
//
// Clase ObjMallaIndexada
//
// *****************************************************************************

GLuint CrearVBO(GLuint tipo_vbo, GLuint tamanio_bytes,
    GLvoid* puntero_ram)
{
    GLuint id_vbo; // resultado: identificador de VBO

    glGenBuffers(1, &id_vbo); // crear nuevo VBO, obtener identificador (nunca 0)
    glBindBuffer(tipo_vbo, id_vbo); // activar el VBO usando su identificador
    // esta instrucción hace la transferencia de datos desde RAM hacia GPU
    glBufferData(tipo_vbo, tamanio_bytes, puntero_ram, GL_STATIC_DRAW);
    glBindBuffer(tipo_vbo, 0); // desactivación del VBO (activar 0)
    return id_vbo; // devolver el identificador resultado
}

// Visualización en modo inmediato con 'glDrawElements'
void ObjMallaIndexada::draw_ModoInmediato(ModoVis modo_vis)
{
    
    // habilitar uso de un array de vértices
    glEnableClientState(GL_VERTEX_ARRAY);

    // indicar el formato y la dirección de memoria del array de vértices
    // (son tuplas de 3 valores float, sin espacio entre ellas)
    glVertexPointer(3, GL_FLOAT, 0, this->vertices.data());
    // visualizar, indicando: tipo de primitiva, número de índices,
    // tipo de los índices, y dirección de la tabla de índices
    
    if (this->id_textura) {
        glEnable(GL_TEXTURE_2D);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
        glActiveTexture(GL_TEXTURE0);
        
        glBindTexture(GL_TEXTURE_2D, this->id_textura);
        glTexCoordPointer(2, GL_FLOAT, 0, this->textura_vertices.data());
        
    }
    
    if (glIsEnabled(GL_LIGHTING)) {
        // indicar vector de normales
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, this->normales_vertices.data());
        aplicar_materiales();
    }else{
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3, GL_FLOAT, 0, this->color.data());
    }
    
    if (modo_vis == ajedrez) {
        for (int i = 0; i < this->triangulos.size(); ++i) {
            if (i % 2 == 0) {
                glColor3f(0.5, 0.5, 0.5);
            } else {
                glColor3f(0.23, 0.0, 0.0);
            }
            glDrawElements(GL_TRIANGLES, 3,
                GL_UNSIGNED_INT, this->triangulos.data() + i);
        }
    } else {
        glDrawElements(GL_TRIANGLES, this->triangulos.size() * 3,
            GL_UNSIGNED_INT, this->triangulos.data());
    }

    // deshabilitar array de vértices
    glDisableClientState(GL_VERTEX_ARRAY);
    
    if (glIsEnabled(GL_LIGHTING)) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }else{
        glDisableClientState( GL_COLOR_ARRAY );
    }

    if (this->id_textura) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
    
}

// -----------------------------------------------------------------------------
// Visualización en modo diferido con 'glDrawElements' (usando VBOs)
void ObjMallaIndexada::draw_ModoDiferido(ModoVis modo_vis)
{
    if (id_vbo_ver == 0) {
        id_vbo_ver = CrearVBO(GL_ARRAY_BUFFER, this->vertices.size() * 3 * sizeof(float), this->vertices.data());
    }
    if (id_vbo_tri == 0) {
        id_vbo_tri = CrearVBO(GL_ELEMENT_ARRAY_BUFFER, this->triangulos.size() * 3 * sizeof(int), this->triangulos.data());
    }

    // especificar localización y formato de la tabla de vértices, habilitar tabla
    glBindBuffer(GL_ARRAY_BUFFER, id_vbo_ver); // activar VBO de vértices\
    // indicar el formato y la dirección de memoria del array de vértices
    // (son tuplas de 3 valores float, sin espacio entre ellas)
    glVertexPointer(3, GL_FLOAT, 0, 0);
    // especifica formato y offset (=0)
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // desactivar VBO de vértices.
    glEnableClientState(GL_VERTEX_ARRAY);
    // habilitar tabla de vértices
    // visualizar triángulos con glDrawElements (puntero a tabla == 0)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_vbo_tri); // activar VBO de triángulos

    if (this->id_textura) {
        glEnable(GL_TEXTURE_2D);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY_EXT);
        glActiveTexture(GL_TEXTURE0);
        
        glBindTexture(GL_TEXTURE_2D, this->id_textura);
        glTexCoordPointer(2, GL_FLOAT, 0, this->textura_vertices.data());
        
    }
    
    if (glIsEnabled(GL_LIGHTING)) {
        // indicar vector de normales
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, 0, this->normales_vertices.data());
        aplicar_materiales();
    }else{
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3, GL_FLOAT, 0, this->color.data());
    }

    if (modo_vis == ajedrez) {
        for (int i = 0; i < triangulos.size(); ++i) {
            if (i % 2 == 0) {
                glColor3f(0.5, 0.5, 0.5);
            } else {
                glColor3f(0.23, 0.0, 0.0);
            }
            glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, (void*)(i * sizeof(int) * 3));
        }
    } else {
        glDrawElements(GL_TRIANGLES, 3 * this->triangulos.size(), GL_UNSIGNED_INT, 0);
    }
    glDrawElements(GL_TRIANGLES, 3 * this->triangulos.size(), GL_UNSIGNED_INT, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    // desactivar VBO de triángulos
    // desactivar uso de array de vértices
    glDisableClientState(GL_VERTEX_ARRAY);
    

    if (glIsEnabled(GL_LIGHTING)) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }else{
        glDisableClientState( GL_COLOR_ARRAY );
    }

    if (this->id_textura) {
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    }
}

// Visualización en modo inmediato con 'glDrawElements'
void ObjMallaIndexada::draw_seleccion(ModoVis modo_vis)
{
    
    // habilitar uso de un array de vértices
    glEnableClientState(GL_VERTEX_ARRAY);

    // indicar el formato y la dirección de memoria del array de vértices
    // (son tuplas de 3 valores float, sin espacio entre ellas)
    glVertexPointer(3, GL_FLOAT, 0, this->vertices.data());
    // visualizar, indicando: tipo de primitiva, número de índices,
    // tipo de los índices, y dirección de la tabla de índices
    
    
    glDrawElements(GL_TRIANGLES, this->triangulos.size() * 3,
        GL_UNSIGNED_INT, this->triangulos.data());
    

    // deshabilitar array de vértices
    glDisableClientState(GL_VERTEX_ARRAY);
}

// -----------------------------------------------------------------------------
// Función de visualización de la malla,
// puede llamar a  draw_ModoInmediato o bien a draw_ModoDiferido
void ObjMallaIndexada::draw(ModoVis modo_vis, bool modo_dibujado)
{
    if (modo_dibujado) {
        draw_ModoDiferido(modo_vis);
    } else {
        draw_ModoInmediato(modo_vis);
    }
    
}

// -----------------------------------------------------------------------------
// Recalcula la tabla de normales de vértices (el contenido anterior se pierde)
void ObjMallaIndexada::calcular_normales()
{
    Tupla3f normal;
    Tupla3f vector1, vector2, triangulo_v1, triangulo_v2, triangulo_v3, vector_cross;
    for (auto& triangulo : triangulos) {
        triangulo_v1 = vertices[triangulo(0)];
        triangulo_v2 = vertices[triangulo(1)];
        triangulo_v3 = vertices[triangulo(2)];

        vector1 = triangulo_v2 - triangulo_v1;
        vector2 = triangulo_v3 - triangulo_v1;

        vector_cross = vector1.cross(vector2);
        normal = vector_cross.normalized();
        normales_caras.emplace_back(normal);
    }

    calcular_normales_vertices();
}

// -----------------------------------------------------------------------------
// Recalcula la tabla de normales de vértices (el contenido anterior se pierde)
void ObjMallaIndexada::calcular_normales_vertices()
{
    normales_vertices.resize(vertices.size());
    for (int i = 0; i < normales_vertices.size(); ++i) {
        normales_vertices[i] = { 0.0, 0.0, 0.0 };
    }

    int v1, v2, v3;
    for (int i = 0; i < normales_caras.size(); ++i) {
        v1 = triangulos[i](0);
        v2 = triangulos[i](1);
        v3 = triangulos[i](2);

        normales_vertices[v1] = normales_vertices[v1] + normales_caras[i];
        normales_vertices[v2] = normales_vertices[v2] + normales_caras[i];
        normales_vertices[v3] = normales_vertices[v3] + normales_caras[i];
    }

    for (int i = 0; i < normales_vertices.size(); ++i) {
        normales_vertices[i] = normales_vertices[i].normalized();
    }
}

// -----------------------------------------------------------------------------
// Aplica el material de cada objeto
void ObjMallaIndexada::aplicar_materiales()
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material.ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.specular);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.shininess);
}

// -----------------------------------------------------------------------------
// Aplica el material de cada objeto
void ObjMallaIndexada::seleccionar()
{
    if (!seleccionado){
        this->material_anterior = this->material;
        this->material = RubberYellow;
        this->color_anterior = this->color;
        color.clear();
        for(auto it : triangulos){
            color.push_back({1.0,1.0,0.0});
        }
        seleccionado = true;
    }else{
        this->material = material_anterior;
        this->color = this->color_anterior;
        seleccionado = false;
    }
}



// *****************************************************************************
//
// Clase Cubo (práctica 1)
//
// *****************************************************************************
Cubo::Cubo()
{
    // inicializar la tabla de vértices
    vertices = {
        { -0.5, -0.5, -0.5 }, // 0
        { -0.5, -0.5, +0.5 }, // 1
        { -0.5, +0.5, -0.5 }, // 2
        { -0.5, +0.5, +0.5 }, // 3
        { +0.5, -0.5, -0.5 }, // 4
        { +0.5, -0.5, +0.5 }, // 5
        { +0.5, +0.5, -0.5 }, // 6
        { +0.5, +0.5, +0.5 } // 7
    };

    // inicializar la tabla de caras o triángulos:
    // (es importante en cada cara ordenar los vértices en sentido contrario
    //  de las agujas del reloj, cuando esa cara se observa desde el exterior del cubo)

    triangulos = { { 0, 2, 4 }, { 4, 2, 6 },
        { 1, 5, 3 }, { 3, 5, 7 },
        { 1, 3, 0 }, { 0, 3, 2 },
        { 5, 4, 7 }, { 7, 4, 6 },
        { 1, 0, 5 }, { 5, 0, 4 },
        { 3, 7, 2 }, { 2, 7, 6 } };

    calcular_normales();
    material = Bronze;

    for(auto it : triangulos){
        color.push_back({1.0,0.5,0.0});
    }
}

// *****************************************************************************
//
// Clase Tetraedro (práctica 1)
//
// *****************************************************************************

Tetraedro::Tetraedro()
{
    vertices = {
        { -0.5, -0.5, -0.5 }, // 0
        { 0.5, 0.5, -0.5 }, // 1
        { -0.5, 0.5, 0.5 }, // 2
        { 0.5, -0.5, 0.5 }, // 3
    };

    triangulos = { { 0, 1, 3 }, { 0, 2, 1 },
        { 0, 3, 2 }, { 3, 1, 2 } };

    calcular_normales();
    material = Chrome;

    for(auto it : triangulos){
        color.push_back({0.5,0.5,1.0});
    }
}

// *****************************************************************************
//
// Clase Cono (práctica 2)
//
// *****************************************************************************

Cono::Cono(const int num_vert_perfil, const int num_instancias_perf)
{
    std::vector<Tupla3f> perfil_original;
    float subdiv = 1.0 / (num_vert_perfil - 1);
    float x_act = 1.0;
    float y_act = -0.5;

    for (int i = 0; i < num_vert_perfil - 1; ++i) {
        perfil_original.emplace_back(x_act, y_act, 0);
        x_act -= subdiv;
        y_act += subdiv;
    }

    crearMalla(perfil_original, num_instancias_perf);
    ponerTapas(0.5, num_instancias_perf);
    
    calcular_normales();
    material = Copper;
    for(auto it : triangulos){
        color.push_back({0.0,1.0,1.0});
    }
}

// *****************************************************************************
//
// Clase Cilindro (práctica 2)
//
// *****************************************************************************

Cilindro::Cilindro(const int num_vert_perfil, const int num_instancias_perf)
{
    std::vector<Tupla3f> perfil_original;
    float subdiv = 1.0 / (num_vert_perfil - 1);
    float y_act = -0.5;

    for (int i = 0; i < num_vert_perfil; ++i) {
        perfil_original.emplace_back(1.0, y_act, 0.0);
        y_act += subdiv;
    }

    crearMalla(perfil_original, num_instancias_perf);
    ponerTapas(0.5, num_instancias_perf);
    calcular_normales();
    material = Gold;
    for(auto it : triangulos){
        color.push_back({1.0,0.0,0.0});
    }
}

// *****************************************************************************
//
// Clase Esfera (práctica 2)
//
// *****************************************************************************

Esfera::Esfera(const int num_vert_perfil, const int num_instancias_perf)
{
    std::vector<Tupla3f> perfil_original;
    float subdiv = 1.0 / (num_vert_perfil - 1);
    float x_act = 0.0;
    float y_act = 0.0;
    float d;

    for (int i = 1; i <= num_vert_perfil; ++i) {
        d = -(M_PI / 2) + (i * M_PI / (num_vert_perfil + 1));
        x_act = 1.0 * cos(d);
        y_act = 1.0 * sin(d);
        perfil_original.emplace_back(x_act, y_act, 0);
    }

    crearMalla(perfil_original, num_instancias_perf);
    ponerTapas(1.0, num_instancias_perf);
    calcular_normales();
    material = Jade;
    for(auto it : triangulos){
        color.push_back({0.3,0.5,1.0});
    }
}

// *****************************************************************************
//
// Clase ObjPLY (práctica 2)
//
// *****************************************************************************
ObjPLY::ObjPLY(const std::string& nombre_archivo)
{
    // leer la lista de caras y vértices
    ply::read(nombre_archivo, vertices, triangulos);
    calcular_normales();
    material = Jade;
    for(auto it : triangulos){
        color.push_back({0.7,0.2,0.2});
    }
}

// *****************************************************************************
//
// Clase ObjRevolucion (práctica 2)
//
// *****************************************************************************
// objeto de revolución obtenido a partir de un perfil (en un PLY)
ObjRevolucion::ObjRevolucion(const std::string& nombre_ply_perfil)
{
    std::vector<Tupla3f> perfil_original;

    // leer la lista de vértices
    ply::read_vertices(nombre_ply_perfil, perfil_original);
    crearMalla(perfil_original, NUM_INSTANCIAS_DEFAULT);
    ponerTapas(0.0, NUM_INSTANCIAS_DEFAULT);
    calcular_normales();
    material = Pearl;
    for(auto it : triangulos){
        color.push_back({0.2,0.4,0.4});
    }
}

void ObjRevolucion::crearMalla(const std::vector<Tupla3f>& perfil_original, const int num_instancias_perf)
{
    int M = perfil_original.size();
    float N = num_instancias_perf * 1.0;

    // Crear tabla de caras
    for (int i = 0; i < num_instancias_perf; ++i) {
        for (auto& vert : perfil_original) {
            vertices.emplace_back(
                vert(0) * cos(2.0 * M_PI * i / N) + vert(2) * sin(2.0 * M_PI * i / N),
                vert(1),
                vert(0) * sin(2.0 * M_PI * i / N) + vert(2) * cos(2.0 * M_PI * i / N));
        }
    }

    // Crear tabla de triangulos
    for (int i = 0; i < num_instancias_perf; ++i) {
        for (int j = 0; j < M - 1; ++j) {
            int a = M * i + j;
            int b = M * ((i + 1) % num_instancias_perf) + j;
            triangulos.emplace_back(a, b + 1, b);
            triangulos.emplace_back(a, a + 1, b + 1);
        }
    }
}

void ObjRevolucion::ponerTapas(float altura, const int num_instancias_perf)
{
    int N = num_instancias_perf;
    int M = vertices.size() / N;

    if (altura == 0.0) {
        for (auto vert : vertices) {
            if (altura < vert(1)) {
                altura = vert(1);
            }
        }
    }

    Tupla3f inf(0.0, -altura, 0.0);
    Tupla3f sup(0.0, altura, 0.0);

    vertices.emplace_back(inf);
    vertices.emplace_back(sup);

    int n_vert = 0;

    triangulos.emplace_back(vertices.size() - 2, M * (N - 1), 0);
    for (int i = 0; i < N - 1; ++i) {
        triangulos.emplace_back(vertices.size() - 2, M * i, M * (i + 1));
    }

    triangulos.emplace_back(vertices.size() - 1, M - 1, M * N - 1);
    for (int i = 0; i < N - 1; ++i) {
        triangulos.emplace_back(vertices.size() - 1, M * (i + 2) - 1, M * (i + 1) - 1);
    }
}



// *****************************************************************************
//
// Clase Cuadro (práctica 4)
//
// *****************************************************************************
Cuadro::Cuadro(const std::string& nombre_archivo, const std::vector<Tupla2f> textura_vertices)
{
    this->textura_vertices = textura_vertices;
    this->nombre_archivo = nombre_archivo;
    

    // inicializar la tabla de vértices
    vertices = {
        { -0.5, -0.5,  0.0 }, // 0
        { -0.5,  0.5,  0.0 }, // 1
        {  0.5,  0.5,  0.0 }, // 2
        {  0.5, -0.5,  0.0 }, // 3
    };

    // inicializar la tabla de caras o triángulos
    triangulos = { { 0, 2, 1 }, { 0, 3, 2 } };

    calcular_normales();
    for(auto it : triangulos){
        color.push_back({0.5,0.6,0.7});
    }
}

void ObjMallaIndexada::preparaTextura()
{
    using namespace cimg_library;

    std::vector<unsigned char> data;
    char * filename = new char [nombre_archivo.length()+1];
    strcpy (filename, nombre_archivo.c_str());

    CImg<unsigned char> textura;
    textura.load(filename);

    // empaquetamos bien los datos
    for (long y = 0; y < textura.height(); y++)
        for (long x = 0; x < textura.width(); x++) {
            unsigned char* r = textura.data(x, y, 0, 0);
            unsigned char* g = textura.data(x, y, 0, 1);
            unsigned char* b = textura.data(x, y, 0, 2);
            data.push_back(*r);
            data.push_back(*g);
            data.push_back(*b);
        }

    glGenTextures(1, &this->id_textura);
    glBindTexture(GL_TEXTURE_2D, this->id_textura);

    glActiveTexture(GL_TEXTURE0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // TRASFIERE LOS DATOS A GPU
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textura.width(), textura.height(),
        0, GL_RGB, GL_UNSIGNED_BYTE, &data[0]);
}