// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#include "camara.h"

Camara::Camara(int tipo, float near, float far, float dzoom){
    this->tipo = tipo;
    this->near = near;
    this->far = far;
    this->eye = {0,0,dzoom};
    this->at = {0,0,0};
    this->up = {0,1,0};
    this->dzoom = dzoom;
}

void Camara::girar(int x, int y){
    x = x%360;
    y = y%360;

    eye[0]= dzoom*sin((-y*M_PI)/180) * cos((-x*M_PI)/180);
    eye[1]= dzoom*sin((x*M_PI)/180);
    eye[2]= dzoom*cos((y*M_PI)/180) * cos((x*M_PI)/180);
    
    if ((x > 90 && x <270) || (x < -90 && x > -270)){
        up[1] = -1;
    }else{
        up[1] = 1;
    }
}

void Camara::zoom(float ratio){

    if(this->dzoom < 10 && ratio > 0 ){
        ratio = 0;
    }
    this->dzoom -= ratio;
    
    this->setProyeccion(left,right);
}

void Camara::setProyeccion(float wx, float wy){

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    this->left = wx;
    this->right = wy;
    
    if(tipo == 0){
        glFrustum(-wx*dzoom, +wx*dzoom, -wy*dzoom, +wy*dzoom, near, far);
    }else{
        glOrtho(-wx*dzoom, +wx*dzoom, -wy*dzoom, +wy*dzoom, near, far);
    }
    
}