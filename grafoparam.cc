// #############################################################################
//
// Informática Gráfica (Grado Informática)
//
// Archivo: GrafoParam.cc
// -- implementación del objeto jerárquico de la práctica 3
//
// #############################################################################

#include "grafoparam.h"

// -----------------------------------------------------------------------------
// constructor: crea mallas indexadas en los nodos terminales del grafo

GrafoParam::GrafoParam()
{
    ankle_l = new ObjPLY("plys/dummy/ankle_l.ply");
    ankle_r = new ObjPLY("plys/dummy/ankle_r.ply");
    elbow_l = new ObjPLY("plys/dummy/elbow_l.ply");
    elbow_r = new ObjPLY("plys/dummy/elbow_r.ply");
    hand_l = new ObjPLY("plys/dummy/hand_l.ply");
    hand_r = new ObjPLY("plys/dummy/hand_r.ply");
    head = new ObjPLY("plys/dummy/head.ply");
    hip_l = new ObjPLY("plys/dummy/hip_l.ply");
    hip = new ObjPLY("plys/dummy/hip.ply");
    hip_r = new ObjPLY("plys/dummy/hip_r.ply");
    hip_tor = new ObjPLY("plys/dummy/hip_tor.ply");
    knee_l = new ObjPLY("plys/dummy/knee_l.ply");
    knee_r = new ObjPLY("plys/dummy/knee_r.ply");
    lower_arm_l = new ObjPLY("plys/dummy/lower_arm_l.ply");
    lower_arm_r = new ObjPLY("plys/dummy/lower_arm_r.ply");
    lower_leg_l = new ObjPLY("plys/dummy/lower_leg_l.ply");
    lower_leg_r = new ObjPLY("plys/dummy/lower_leg_r.ply");
    neck = new ObjPLY("plys/dummy/neck.ply");
    shoulder_l = new ObjPLY("plys/dummy/shoulder_l.ply");
    shoulder_r = new ObjPLY("plys/dummy/shoulder_r.ply");
    feet_l = new ObjPLY("plys/dummy/feet_l.ply");
    feet_r = new ObjPLY("plys/dummy/feet_r.ply");
    body = new ObjPLY("plys/dummy/torso.ply");
    upper_arm_l = new ObjPLY("plys/dummy/upper_arm_l.ply");
    upper_arm_r = new ObjPLY("plys/dummy/upper_arm_r.ply");
    upper_leg_l = new ObjPLY("plys/dummy/upper_leg_l.ply");
    upper_leg_r = new ObjPLY("plys/dummy/upper_leg_r.ply");
    wrist_l = new ObjPLY("plys/dummy/wrist_l.ply");
    wrist_r = new ObjPLY("plys/dummy/wrist_r.ply");

    for( unsigned i = 0 ; i < num_parametros-1 ; i++ ){
        body_rotations.push_back(0.0);
    }
    head_size = 1.0;
}
// -----------------------------------------------------------------------------
// actualizar valor efectivo de un parámetro (a partir de su valor no acotado)

void GrafoParam::actualizarValorEfe(const unsigned iparam, const float valor_na)
{
    assert(iparam < num_parametros);

    using namespace std;

    constexpr float vp = 2.5;

    switch (iparam) {
    case 13:
        head_size = 1.0 + 0.25 * sin(3.0 * (2.0 * M_PI * valor_na));
        break;
    case 14:
        // BODY Y POSITION
        fullbody_y = 0.01 + 0.05 * sin(3.0 * (2.0 * M_PI * valor_na));
        break;
    case 15:
        // WALK ANIMATION
        upper_leg_rotation =  50 * sin(1.5 * (2.0 * M_PI * valor_na));
        left_lower_leg_rotation = 50 -  50 * sin(1.5 * (2.0 * M_PI * valor_na));
        right_lower_leg_rotation = 50+ 50 * sin(1.5 * (2.0 * M_PI * valor_na));
        break;
    default:
        body_rotations[iparam] = valor_na*50;
        break;
    }
}

// -----------------------------------------------------------------------------
// visualización del objeto Jerárquico con OpenGL,
// mediante llamadas a los métodos 'draw' de los sub-objetos

void GrafoParam::draw(const ModoVis p_modo_vis, const bool p_usar_diferido)
{
    // guardar parametros de visualización para esta llamada a 'draw'
    // (modo de visualización, modo de envío)
    modo_vis = p_modo_vis;
    usar_diferido = p_usar_diferido;

    // Dibujar figura entera
    fullBody();
}

// -----------------------------------------------------------------------------
// dibuja la cabeza y el cuello
void GrafoParam::head_full()
{
    glPushMatrix();
        // ** PART  : neck --->::: 0x, 0.85y, 0z
        // ** EDITS
        glTranslatef(0, 0.85, 0.011);
        glRotatef( body_rotations[2], 0.0, 0.0, 1.0 );
        glRotatef( body_rotations[1], 0.0, 1.0, 0.0 );
        glRotatef( body_rotations[0], 1.0, 0.0, 0.0 );
        glScalef( head_size, head_size, head_size);
        glTranslatef(0, -0.85, -0.011);
        // DRAW
        head->draw(modo_vis, usar_diferido);
        neck->draw(modo_vis, usar_diferido);
        // **
    glPopMatrix();

}

// -----------------------------------------------------------------------------
// dibuja el brazo derecho entero
void GrafoParam::arm_r()
{
    glPushMatrix();
        // ** PART  : shoulder_r --->::: -0.11x, 0.78y, 0.013z
        // ** EDITS
        glTranslatef(-0.11, 0.78, 0.013);
        glRotatef( body_rotations[5], 0.0, 0.0, 1.0 );
        glRotatef( body_rotations[4], 0.0, 1.0, 0.0 );
        glRotatef( body_rotations[3], 1.0, 0.0, 0.0 );
        glTranslatef(+0.11, -0.78, -0.013);
        shoulder_r->draw(modo_vis, usar_diferido);
        // **
        glPushMatrix();
            upper_arm_r->draw(modo_vis, usar_diferido);
            glPushMatrix();
                // ** PART  : elbow_r --->::: -0.26x, 0.78y, 0.013z
                // ** EDITS
                glTranslatef(-0.26, 0.78, 0.013);
                glRotatef( body_rotations[6], 0.0, 1.0, 0.0 );
                glTranslatef(+0.26, -0.78, -0.013);
                elbow_r->draw(modo_vis, usar_diferido);
                // **
                glPushMatrix();
                    lower_arm_r->draw(modo_vis, usar_diferido);
                    glPushMatrix();
                        // ** PART  : wrist_r, hand_r --->::: -0.41x, 0.78y, 0.013z
                        // ** EDITS
                        //glTranslatef(-0.41, 0.78, 0.013);
                        //glTranslatef(+0.41, -0.78, -0.013);
                        wrist_r->draw(modo_vis, usar_diferido);
                        hand_r->draw(modo_vis, usar_diferido);
                        // **
                    glPopMatrix();
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

// -----------------------------------------------------------------------------
// dibuja el brazo izquierdo entero
void GrafoParam::arm_l()
{
    glPushMatrix();
        // ** PART  : shoulder_l --->::: 0.11x, 0.78y, 0.013z
        // ** EDITS
        glTranslatef(0.11, 0.78, 0.013);
        glRotatef( body_rotations[9], 0.0, 0.0, 1.0 );
        glRotatef( body_rotations[8], 0.0, 1.0, 0.0 );
        glRotatef( body_rotations[7], 1.0, 0.0, 0.0 );
        glTranslatef(-0.11, -0.78, -0.013);
        shoulder_l->draw(modo_vis, usar_diferido);
        // **
        glPushMatrix();
            upper_arm_l->draw(modo_vis, usar_diferido);
            glPushMatrix();
                // ** PART  : elbow_l --->::: 0.26x, 0.78y, 0.013z
                // ** EDITS
                glTranslatef(0.26, 0.78, 0.013);
                glRotatef( body_rotations[10], 0.0, 1.0, 0.0 );
                glTranslatef(-0.26, -0.78, -0.013);
                // ** DRAW
                elbow_l->draw(modo_vis, usar_diferido);
                //
                glPushMatrix();
                    lower_arm_l->draw(modo_vis, usar_diferido);
                    glPushMatrix();
                        wrist_l->draw(modo_vis, usar_diferido);
                        hand_l->draw(modo_vis, usar_diferido);
                    glPopMatrix();
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

// -----------------------------------------------------------------------------
// dibuja la parte superior del cuerpo (torso, brazos y cabeza)
void GrafoParam::upper_body()
{
    glPushMatrix();
        // ** PART  : hip --->::: 0x, 0.525y, 0.011z
        // ** EDITS
        glTranslatef(0, 0.525, 0.011);
        glRotatef( body_rotations[12], 1.0, 0.0, 0.0 );
        glTranslatef(0, -0.52, -0.011);
        // ** DRAW
        glPushMatrix();
            glPushMatrix();
                // ** PART  : hip_tor --->::: 0.26x, 0.65y, 0.013z
                // ** EDITS
                glTranslatef(0.26, 0.65, 0.011);
                glRotatef( body_rotations[11], 1.0, 0.0, 0.0 );
                glTranslatef(-0.26, -0.65, -0.011);
                // ** DRAW
                head_full();
                body->draw(modo_vis, usar_diferido);
                arm_l();
                arm_r();
                hip_tor->draw(modo_vis, usar_diferido);
            glPopMatrix();
            hip->draw(modo_vis, usar_diferido);
        glPopMatrix();
    glPopMatrix();
}

// -----------------------------------------------------------------------------
// dibuja la pierna derecha
void GrafoParam::leg_r()
{
    // Hip
    glPushMatrix();
        // ** PART  : leg_r --->::: -0.044x, 0.52y, 0.04z
        // ** EDITS
        glTranslatef(-0.044, 0.52, 0.02);
        glRotatef( upper_leg_rotation, 1.0, 0.0, 0.0 );
        glTranslatef(0.044, -0.52, -0.02);
        // ** DRAW
        hip_r->draw(modo_vis, usar_diferido);
        glPushMatrix();
            upper_leg_r->draw(modo_vis, usar_diferido);
            // Knee
            glPushMatrix();
                // ** PART  : knee_r --->::: -0.044x, 0.27y, 0.015z
                // ** EDITS
                glTranslatef(-0.044, 0.27, 0.015);
                glRotatef( right_lower_leg_rotation, 1.0, 0.0, 0.0 );
                glTranslatef(0.044, -0.27, -0.015);
                // ** DRAW
                knee_r->draw(modo_vis, usar_diferido);
                glPushMatrix();
                    lower_leg_r->draw(modo_vis, usar_diferido);
                    // Ankle
                    glPushMatrix();
                        ankle_r->draw(modo_vis, usar_diferido);
                        feet_r->draw(modo_vis, usar_diferido);
                    glPopMatrix();
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

// -----------------------------------------------------------------------------
// dibuja la pierna izquierda
void GrafoParam::leg_l()
{
    // Hip
    glPushMatrix();
        // ** PART  : leg_l --->::: 0.044x, 0.52y, 0.04z
        // ** EDITS
        glTranslatef(0.044, 0.52, 0.02);
        glRotatef( -upper_leg_rotation, 1.0, 0.0, 0.0 );
        glTranslatef(-0.044, -0.52, -0.02);
        // ** DRAW
        hip_l->draw(modo_vis, usar_diferido);
        glPushMatrix();
            upper_leg_l->draw(modo_vis, usar_diferido);
            // Knee
            glPushMatrix();
                // ** PART  : knee_r --->::: -0.044x, 0.27y, 0.015z
                // ** EDITS
                glTranslatef(0.044, 0.27, 0.015);
                glRotatef( left_lower_leg_rotation, 1.0, 0.0, 0.0 );
                glTranslatef(-0.044, -0.27, -0.015);
                // ** DRAW
                knee_l->draw(modo_vis, usar_diferido);
                glPushMatrix();
                    lower_leg_l->draw(modo_vis, usar_diferido);
                    // Ankle
                    glPushMatrix();
                        ankle_l->draw(modo_vis, usar_diferido);
                        feet_l->draw(modo_vis, usar_diferido);
                    glPopMatrix();
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

// -----------------------------------------------------------------------------
// dibuja el cuerpo entero
void GrafoParam::fullBody()
{
    glPushMatrix();
        glTranslatef(0.0, fullbody_y, 0.0 );
        upper_body();
        leg_r();
        leg_l();
    glPopMatrix();
}




