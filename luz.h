// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#ifndef LUZ_H_INCLUDED
#define LUZ_H_INCLUDED

#include "tuplasg.h"
#include "aux.h"

class Luz{

public:
    Luz(GLenum modo, Tupla4f posicion, Tupla4f ambiente, Tupla4f difusa, Tupla4f especular);
    void conmutar(bool activa);
    void rotar(int angulo, unsigned int x, unsigned int y, unsigned int z);
private:
    GLenum modo;
    Tupla4f posicion;
    Tupla4f ambiente;
    Tupla4f difusa;
    Tupla4f especular;
    bool activa;
    int angulo = 0;
    int rotX = 0;
    int rotY = 0;
    int rotZ = 0;
    void set_rotation();
};

#endif