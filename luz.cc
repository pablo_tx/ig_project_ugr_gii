// #############################################################################
//
// Nombre: Pablo Pozo Tena      Grupo: A2
//
// #############################################################################
#include "luz.h"

Luz::Luz(GLenum modo, Tupla4f posicion, Tupla4f ambiente, Tupla4f difusa, Tupla4f especular){
    this->modo = modo;
    this->posicion = posicion;
    this->ambiente = ambiente;
    this->difusa = difusa;
    this->especular = especular;
}

void Luz::conmutar(bool activa){

    if(activa){
        glEnable(modo);
        glLightfv(modo, GL_POSITION, posicion);
        glLightfv(modo, GL_AMBIENT, ambiente);
        glLightfv(modo, GL_DIFFUSE, difusa);
        glLightfv(modo, GL_SPECULAR, especular);
        set_rotation();
    }else{
        glDisable(modo);
    }
}

void Luz::rotar(int angulo, unsigned int x, unsigned int y, unsigned int z){
    assert(x == 1 || x == 0);
    assert(y == 1 || y == 0);
    assert(z == 1 || z == 0);

    this->rotX = x;
    this->rotY = y;
    this->rotZ = z;

    this->angulo = (this->angulo + angulo) % 360;
    set_rotation();
}

void Luz::set_rotation(){
    glPushMatrix();
        glRotatef(this->angulo, rotX, rotY, rotZ);
        glLightfv(modo, GL_POSITION, posicion);
    glPopMatrix();
}